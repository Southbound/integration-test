const { request } = require('express');
const express = require('express');
const converter = require('./converter');
const app = express();
const port = 3000;

// endpoint localhost:3000/
app.get('/', (req, res) => res.send("Server online"));

// endpoint localhost:3000/rgb-to-hex?red=255&green=0&blue=0
app.get('/rgb-to-hex', (req, res) => {
  const red = parseInt(req.query.red, 10);
  const green = parseInt(req.query.green, 10);
  const blue = parseInt(req.query.blue, 10);
  res.send(converter.rgbToHex(red, green, blue));
})

// endpoint localhost:3000/hex-to-rgb?hex=ffffff
app.get('/hex-to-rgb', (req, res) => {
  const hex = req.query.hex
  const rgb = converter.hexToRgb(hex);
  res.send(rgb);
})

if (process.env.NODE_ENV === 'test') {
  module.exports = app;
} else {
  app.listen(port, () => console.log(`Server listening on localhost:${port}`))
}