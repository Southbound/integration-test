const pad = (hex) => {
  return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
  /*   rgbToHex: (red, green, blue) => {
      const redHex = red.toString(16); // "00"
      const greenHex = green.toString(16);
      const blueHex = blue.toString(16);

      return pad(redHex) + pad(greenHex) + pad(blueHex); // "000000"
    },
   */
  hexToRgb: (hex) => {
    const redRgb = parseInt(hex.substring(0, 2), 16);
    const greenRgb = parseInt(hex.substring(2, 4), 16);
    const blueRgb = parseInt(hex.substring(4, 6), 16);

    return [redRgb, greenRgb, blueRgb]; // "0, 0, 0"
  }
}