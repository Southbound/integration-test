const expect = require('chai').expect;
const converter = require('../src/converter');

describe("Color code converter", () => {
/*   describe("RGB to Hex conversion", () => {
    it("Converts the basic colours", () => {
      const redHex = converter.rgbToHex(255, 0, 0);   // ff0000
      const greenHex = converter.rgbToHex(0, 255, 0); // 00ff00
      const blueHex = converter.rgbToHex(0, 0, 255);  // 0000ff

      expect(redHex).to.equal("ff0000")
      expect(greenHex).to.equal("00ff00")
      expect(blueHex).to.equal("0000ff")
    })
  }); */
  describe("Hex to RGB conversion", () => {
    it("Converts the basic colours", () => {
      var redRgb = converter.hexToRgb("ff0000");    // 255,   0,   0
      var greenRgb = converter.hexToRgb("00ff00");  //   0, 255,   0
      var blueRgb = converter.hexToRgb("0000ff");   //   0,   0, 255

      expect(redRgb).to.deep.equal([255,0,0])
      expect(greenRgb).to.deep.equal([0,255,0])
      expect(blueRgb).to.deep.equal([0,0,255])
    })
  })
})