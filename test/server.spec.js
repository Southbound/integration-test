const expect = require('chai').expect;
const request = require('request');
const app = require('../src/server');
const port = 3000;

describe("Color code converter API", () => {
  before("Start server", (done) => {
    server = app.listen(port, () => {
      console.log(`Server listening on localhost:${port}`)
      done();
    })
  })
/*   describe("RGB to Hex conversion", () => {
    const url = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`;
    it("Returns status code 200", (done) => {
      request(url, (error, response, body) => {
        expect(response.statusCode).to.equal(200);
        done();
      })
    });
    it("Returns the color in Hex", (done) => {
      request(url, (error, response ,body) => {
        expect(body).to.equal("ffffff");
        done();
      })
    })
  })
 */
  describe("Hex to RGB conversion", () => {
    const url = `http://localhost:${port}/hex-to-rgb?hex=ffffff`;
    it("Returns status code 200", (done) => {
      request(url, (error, response, body) => {
        expect(response.statusCode).to.equal(200);
        done();
      })
    });
    it("Returns the color in RGB", (done) => {
      request(url, (error, response ,body) => {
        expect(body).to.equal("[255,255,255]");
        done();
      })
    })
  })

  after("Close server", (done) => {
    server.close();
    done();
  })
})